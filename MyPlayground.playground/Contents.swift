import Cocoa

//For each
let names = ["Tony", "Uncle Jun", "Med", "Ryan"]
for eachName in names {
    print("Ciao \(eachName)!")
    
}

print("\n")

//For each with 2D
let citys = ["New Jersey" : "Jonny", "New York" : "Tony"]
for (ownCity, boss) in citys {
    print("\(boss) owns \(ownCity)")
}

print("\n")

//For-ins
for index in 1...5 {
    print ("\(index) times 5 is \(index * 5)")
}

print("\n")

//For Range
let bullets = 5
for shots in 0..<bullets {
    print("shot # \(shots + 1)")
}

print("\n")

//For-Stride (Interval) Open
let mins = 60
let interval = 5
for tickMark in stride(from: 0, to: mins, by: interval) {
    print("Tick tock: \(tickMark)")
}

print("\n")

//For-Stride (Interval) Closed
let hours = 12
let hInternal = 3
for hMark in stride(from: 3, through: hours, by: hInternal) {
    print("Hour interval \(hMark)")
}
